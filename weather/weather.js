const axios = require ('axios');

const getWeather = async (latitude, lenght) => {
    const resp = await axios.get(`https://api.openweathermap.org/data/2.5/weather?lat=${latitude}&lon=${lenght}&appid=589fa679d4bd20f4a5e82f41085c428c`)

    return resp.data.main.temp -273.15 ;
    
}

module.exports={
    getWeather
}