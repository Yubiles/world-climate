const axios = require('axios');
const argv = require('../config/yargs').argv

const getLatitudeLenght = async (address) =>{
    const  encodedUlr =  encodeURI (address); 

    const instance = axios.create({
        baseURL: `https://devru-latitude-longitude-find-v1.p.rapidapi.com/latlon.php?location=${encodedUlr}`,
        timeout: 1000,
        headers: {'X-RapidAPI-Key': 'f3cb6731a8mshe6ffeddc3119d08p10a2b1jsnc2e70bb07d88'}
    });

    const resp = await instance.get();

    if (resp.data.Results.lenght === 0){
        throw new Error (`SORRY! it was not possible to find results for ${address}`);
    }
     const data = resp.data.Results[0];
     const place = data.name;
     const latitude = data.lat;
     const lenght = data.lon;

    return{
        place,
        latitude,
        lenght
    }
}

module.exports={
    getLatitudeLenght
}

