const argv = require('./config/yargs').argv
const CityLocation = require ('./location/location');
const weather = require('./weather/weather');

// CityLocation.getLatitudeLenght(argv.adress)
//     .then(console.log);

// weather.getWeather(37.560001,126.989998)
//     .then(console.log)
//     .catch(console.log);

const  getInfo = async(adress)=>{

    try{
        const coordinates = await CityLocation.getLatitudeLenght(adress);
        const temp = await weather.getWeather(coordinates.latitude, coordinates.lenght)

        return `El clima de  ${coordinates.place} es de ${temp}`;
    }catch(e){
        return `No se pudo determinar el clima ${adress}`;
    }
}

getInfo (argv.adress)
    .then(console.log)
    .catch(console.log);
